

public class Arc{
    int flot;
    int capa;

    public Arc(int f, int c){
        this.flot = f;
        this.capa = c;
    }

    public String toString(){
        return " "+this.flot+"/"+this.capa+" ";
    }

    // capacité résiduelle de l'arc
    public int Cf(){
        return this.capa-this.flot;
    }
}