import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Graphe2Flot{
    int n; // nombre de sommets
    int indFirstTeamNode; // indice du sommet correspondant au premier team node du graphe
    int nbMatchsEntreAutresEquipes;
    Sommet[] sommets;
    Arc[][] matrice;

    // données venant du fichier
    int nbEquipes;
    String[] nomsEquipes;
    int[] nbVictoires;
    int[] matchsRestants;
    int[][] matchsRestantsEntreEquipes;

    /**
     * @return Le graphe initialisé avec un fichier.
     */
    public static Graphe2Flot parseur(){
        Scanner in = new Scanner(System.in);
        System.out.println("Saisir fichier : ");
        String fichier = in.nextLine();
        try{
            in = new Scanner(new File(fichier));
        }
        catch(FileNotFoundException e){
            e.printStackTrace();
        }

        int nbEquipe = in.nextInt();
        in.nextLine();

        // données venant du fichier
        String[] nomsEquipes = new String[nbEquipe];
        int[] nbVictoires = new int[nbEquipe];
        int[] matchsRestants = new int[nbEquipe];
        int[][] matchsRestantsEntreEquipes = new int[nbEquipe][nbEquipe];
        for(int i=0; i<nbEquipe;i++){
            //on récupère les données 
            in.nextInt();
            nomsEquipes[i] = in.next();
            nbVictoires[i] = in.nextInt();
            matchsRestants[i] = in.nextInt();

            for(int j=0;j<nbEquipe;j++){
                matchsRestantsEntreEquipes[i][j]= in.nextInt();

            }
        }

        return new Graphe2Flot(nbEquipe, nbVictoires, matchsRestants, matchsRestantsEntreEquipes, nomsEquipes);
    }

    /**
     * Initialise le graphe.
     * @param nbEquipes Le nombre total d'équipes.
     * @param nbVic Les nombres de victoires actuelles pour chaque équipes.
     * @param matchsRestants Les nombres de matchs restants actuel pour chaque équipes.
     * @param matchsRestantsEntreEquipes Les nombres de matchs restants actuel entre chaque équipes.
     */
    public Graphe2Flot(int nbEquipes, int[] nbVic, int[] matchsRestants, int[][] matchsRestantsEntreEquipes, String[] equipes){
        this.nbEquipes = nbEquipes;
        this.nbVictoires = nbVic;
        this.matchsRestants = matchsRestants;
        this.matchsRestantsEntreEquipes = matchsRestantsEntreEquipes;
        this.nomsEquipes = equipes;
        nbEquipes--;
        this.nbMatchsEntreAutresEquipes = ((nbEquipes*nbEquipes)-nbEquipes)/2;
        this.n = 2 + nbMatchsEntreAutresEquipes + nbEquipes;
        this.matrice = new Arc[n][n];
        this.sommets = new Sommet[n];
        for(int i=0;i<this.n;i++){
            this.sommets[i] = new Sommet();
        }
    }

    public String toString(){
        String str = "";
        for(int i=0;i<this.n;i++){
            str += "S["+i+"]: "+sommets[i].toString()+"\n";
        }
        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                if(matrice[i][j]!=null){
                    str += matrice[i][j].toString();
                }
                else{
                    str += "  -  ";
                }
            }
            str += "\n";
        }
        return str;
    }

    /**
     * Consruit le réseau pour tester si l'équipe k est éliminé ou non.
     * @param k Le numéro de l'équipe du test.
     */
    private void ConstructionReseau(int k){
        // k est le numéro de l'équipe en partant de 0
        // arcs de s vers games nodes
        this.indFirstTeamNode = 1;
        for(int i=0;i<this.nbEquipes;i++){
            for(int j=i+1;j<this.nbEquipes;j++){
                if(i!=k && j!=k){
                    matrice[0][this.indFirstTeamNode] = new Arc(0, matchsRestantsEntreEquipes[i][j]);
                    this.indFirstTeamNode++;
                }
            }
        }
        // arcs de games nodes vers team nodes
        int indTeam1 = this.indFirstTeamNode;
        int indTeam2 = this.indFirstTeamNode+1;
        for(int i=1;i<this.indFirstTeamNode;i++){
            matrice[i][indTeam1] = new Arc(0, Integer.MAX_VALUE);
            matrice[i][indTeam2] = new Arc(0, Integer.MAX_VALUE);
            indTeam2++;
            if(indTeam2>=this.indFirstTeamNode+this.nbEquipes-1){
                indTeam1++;
                indTeam2 = indTeam1+1;
            }
        }
        // arcs de team nodes vers t
        for(int i=this.indFirstTeamNode;i<this.indFirstTeamNode+this.nbEquipes-1;i++){
            matrice[i][this.n-1] = new Arc(0, nbVictoires[k]+matchsRestants[k]-nbVictoires[i-indFirstTeamNode]);
        }
    }

    /**
     * Initialise le graphe pour pouvoir exécuter l'algo de Préflot.
     */
    private void initPreflot(){
        for(Sommet s : sommets){
            s.hauteur = 0;
            s.excedent = 0;
        }
        sommets[0].hauteur = this.n;
        for(int i=0;i<this.n;i++){
            for(int j=i+1;j<this.n;j++){
                if(matrice[i][j]!=null){
                    matrice[i][j].flot = 0;
                    matrice[j][i] = new Arc(0, 0);
                }
            }
        }
        int capa = 0;
        for(int i=1;i<this.indFirstTeamNode;i++){
            capa = matrice[0][i].capa;
            matrice[0][i].flot = capa;
            matrice[i][0] = new Arc(-capa, 0);
            sommets[i].excedent = capa;
            sommets[0].excedent -= capa;
        }
    }
   
    /**
     * Une des méthode de l'algo de Préflot permettant de déverser de u vers v.
     * @param u sommet donneur.
     * @param v sommet receveur.
     * @return vrai si la méthode avancer peut être exécuter, faux sinon.
     */
    private Boolean avancer(int u, int v){
        if(u!=0 && u!= this.n-1 && matrice[u][v]!=null && sommets[u].excedent > 0 && matrice[u][v].Cf() > 0 && sommets[u].hauteur == sommets[v].hauteur+1){// précondition
            int flotAjoute = Math.min(sommets[u].excedent, matrice[u][v].Cf());
            matrice[u][v].flot += flotAjoute;
            matrice[v][u].flot = -matrice[u][v].flot;
            sommets[u].excedent -= flotAjoute;
            sommets[v].excedent += flotAjoute;
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * Une des méthode de l'algo de Préflot permettant d'augmenter la hauteur de u.
     * @param u sommet a élever.
     * @return vrai si la méthode elever peut être exécuter, faux sinon.
     */
    private Boolean elever(int u){
        if(u!=0 && u!= this.n-1 && sommets[u].excedent>0){// précondition
            int minHauteurVoisin = Integer.MAX_VALUE;
            for(int i=0;i<this.n;i++){
                if(matrice[u][i]!=null && minHauteurVoisin>sommets[i].hauteur && matrice[u][i].Cf()>0){
                    minHauteurVoisin = sommets[i].hauteur;
                }
                if(matrice[i][u]!=null && minHauteurVoisin>sommets[i].hauteur && matrice[u][i].Cf()>0){
                    minHauteurVoisin = sommets[i].hauteur;
                }
            }
            if(minHauteurVoisin>=sommets[u].hauteur){// précondition
                sommets[u].hauteur = 1+minHauteurVoisin;
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }

    /**
     * Calcul le flot max de this en utilisant l'algo de préflot.
     * n² fois l'opération élever qui coute O(n) + n²(n+m) opérations avancer qui coute O(1)
     * le problème c'est de faire les tests sur des sommets qui ne peuvent pas changer
     */
    public void Preflot(){
        this.initPreflot();
        Boolean again = true;
        while(again){
            again = false;
            for(int i=0;i<this.n;i++){
                if(this.elever(i)){
                    again = true;
                    for(int j=0;j<this.n;j++){
                        if(this.avancer(i,j)){
                            again = true;
                        }
                    }
                }
                else{
                    for(int j=0;j<this.n;j++){
                        if(this.avancer(j,i)){
                            again = true;
                        }
                    }
                }
            }
        }
    }

    Boolean isUnSommetActif(){
        for(int i=0;i<this.n-1;i++){ // le dernier c'est le puit
            if(sommets[i].excedent>0){
                return true;
            }
        }
        return false;
    } 

    /**
     * Calcul le flot max de this en utilisant l'algo de préflot.
     */
    public void Preflot2(){
        this.initPreflot();
        while(isUnSommetActif()){
            for(int i=0;i<this.n;i++){
                if(this.elever(i)){
                    for(int j=0;j<this.n;j++){
                        this.avancer(i,j);
                    }
                }
                else{
                    for(int j=0;j<this.n;j++){
                        this.avancer(j,i);
                    }
                }
            }
        }
    }


    /**
     * Test si l'équiê k est éliminé ou non.
     * @param k equipe de test.
     * @return vrai si l'équipe k est éliminé, faux sinon.
     */
    public Boolean TestEliminationEquipe(int k){
        k--; // le k en entrée est entre 1 et nbEquipsp inclusp
        this.ConstructionReseau(k);
        this.Preflot2();
        for(Arc a: matrice[0]){
            if(a!=null && a.flot!=a.capa){
                return true;
            }
        }
        return false;
    }

    /**
     * Test pour toutes les équipes si elles sont éliminés ou non.
     * @return tableau de booléen avec vrai si l'équipe d'indice i est éliminé, faux sinon
     */
    public Boolean[] TestEliminationToutes(){
        Boolean[] result = new Boolean[this.nbEquipes];
        for(int i=0;i<this.nbEquipes;i++){
            if(result[i]==null){
                result[i] = TestEliminationEquipe(i+1);
                System.out.print("Équipe "+this.nomsEquipes[i]+"("+(i+1)+")"+" éliminé ? "+result[i]);
            }
            if(result[i]){ // Lemme d'élimination directe
                for(int j=i+1;j<this.nbEquipes;j++){
                    if(nbVictoires[i]+matchsRestants[i]>=nbVictoires[j]+matchsRestants[j]){
                        result[j]=true;
                        System.out.print(" => équipe "+this.nomsEquipes[j]+"("+(j+1)+")"+" éliminé (Lemme)");
                    }
                }
            }
            System.out.println();
        }
        return result;
    }
}

