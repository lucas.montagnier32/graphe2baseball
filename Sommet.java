public class Sommet {
    int hauteur;
    int excedent;

    public Sommet(){
        this.hauteur = 0;
        this.excedent = 0;
    }

    public String toString(){
        return "h="+this.hauteur+" e="+this.excedent;
    }
}
