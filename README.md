# Graphes2Baseball
Ce projet a pour but de savoir quelles équipes ont déjà été éliminées à un moment donné d'un championnat de baseball en modélisant le problème par un graphe de flot. Le flot maximum dans ces graphes nous donne la réponse si une équipe est déjà éliminées ou non.

### Contributeurs
- Dorian Lauwerier
- Lucas Montagnier
